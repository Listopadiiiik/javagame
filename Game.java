import java.util.Scanner;

public class Game {
    
public static String[][] symbols = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}}; 
public static int ox, oy, xx, xy;
    
    public static void main(String[] args) {

        int random = (int)(Math.random() * (10 - 1) + 1);
        
        if (random > 5){
            int counter = 0;
            do {
                if (symbols[0][0] == "X" && symbols[1][0] == "X" && symbols[2][0] == "X" ||
                
                    symbols[1][0] == "X" && symbols[1][1] == "X" && symbols[1][2] == "X" || 
                    symbols[2][0] == "X" && symbols[2][1] == "X" && symbols[2][2] == "X" || 
                    symbols[0][0] == "X" && symbols[1][0] == "X" && symbols[2][0] == "X" || 
                    symbols[0][1] == "X" && symbols[1][1] == "X" && symbols[2][1] == "X" ||
                    symbols[2][0] == "X" && symbols[2][1] == "X" && symbols[2][2] == "X" ||
                    symbols[0][0] == "X" && symbols[1][1] == "X" && symbols[2][2] == "X" || 
                    symbols[2][0] == "X" && symbols[1][1] == "X" && symbols[0][2] == "X"){
                    System.out.println("X won");
                    break;
                } else if ( symbols[0][0] == "O" && symbols[1][0] == "O" && symbols[2][0] == "O" ||
                            symbols[1][0] == "O" && symbols[1][1] == "O" && symbols[1][2] == "O" || 
                            symbols[2][0] == "O" && symbols[2][1] == "O" && symbols[2][2] == "O" || 
                            symbols[0][0] == "O" && symbols[1][0] == "O" && symbols[2][0] == "O" || 
                            symbols[0][1] == "O" && symbols[1][1] == "O" && symbols[2][1] == "O" || 
                            symbols[2][0] == "O" && symbols[2][1] == "O" && symbols[2][2] == "O" || 
                            symbols[0][0] == "O" && symbols[1][1] == "O" && symbols[2][2] == "O" || 
                            symbols[2][0] == "O" && symbols[1][1] == "O" && symbols[0][2] == "O"){
                                System.out.println("O won");
                                break;
                } else {
                    printSymX();
                    printSymO();
                    counter++;
                }
            } while (counter < 4);

        }
        if (random < 5){
            int counter = 0;
                do {
                    if (symbols[0][0] == "X" && symbols[1][0] == "X" && symbols[2][0] == "X" ||
                    symbols[1][0] == "X" && symbols[1][1] == "X" && symbols[1][2] == "X" || 
                    symbols[2][0] == "X" && symbols[2][1] == "X" && symbols[2][2] == "X" || 
                    symbols[0][0] == "X" && symbols[1][0] == "X" && symbols[2][0] == "X" || 
                    symbols[0][1] == "X" && symbols[1][1] == "X" && symbols[2][1] == "X" ||
                    symbols[2][0] == "X" && symbols[2][1] == "X" && symbols[2][2] == "X" ||
                    symbols[0][0] == "X" && symbols[1][1] == "X" && symbols[2][2] == "X" || 
                    symbols[2][0] == "X" && symbols[1][1] == "X" && symbols[0][2] == "X"){
                    System.out.println("X won");
                    break;
                } else if ( symbols[0][0] == "O" && symbols[1][0] == "O" && symbols[2][0] == "O" ||
                            symbols[1][0] == "O" && symbols[1][1] == "O" && symbols[1][2] == "O" || 
                            symbols[2][0] == "O" && symbols[2][1] == "O" && symbols[2][2] == "O" || 
                            symbols[0][0] == "O" && symbols[1][0] == "O" && symbols[2][0] == "O" || 
                            symbols[0][1] == "O" && symbols[1][1] == "O" && symbols[2][1] == "O" || 
                            symbols[2][0] == "O" && symbols[2][1] == "O" && symbols[2][2] == "O" || 
                            symbols[0][0] == "O" && symbols[1][1] == "O" && symbols[2][2] == "O" || 
                            symbols[2][0] == "O" && symbols[1][1] == "O" && symbols[0][2] == "O"){
                                System.out.println("O won");
                                break;
                } else {
                        printSymO();
                        printSymX();
                        counter++;
                    }
                } while(counter < 4);
            }
        }
    
    public static void printSymO(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Print O row");
                ox = sc.nextInt();
                System.out.println("Print O column");
                oy = sc.nextInt();
                symbolO.symbolO(symbols, ox, oy);
    }
    public static void printSymX(){
            Scanner sc = new Scanner(System.in);
                System.out.println("Print X row");
                int xx = sc.nextInt();
                System.out.println("Print X column");
                int xy = sc.nextInt();
                symbolX.symbolX(symbols, xx, xy);
    }
}

class symbolX  {
    static String[][] symbols = Game.symbols;
    static int row, column;
    public static void symbolX(String[][] a, int b, int c){
        row = b;
        column = c;
        if (a[row][column] != "O" && a[row][column] != "X"){
            a[row][column] = "X";
            Print(a);
        
        } else if (a[row][column] != " ") {
            System.out.println("It is occupied!");
            Game.printSymX();
        } 

    }
    public static void Print(String[][] a){
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                    System.out.print("|" + a[i][j] + "|");
            }
            System.out.println();
        }
    }
}


class symbolO  {
    static String[][] symbols = Game.symbols;
    static int row, column;
    public static void symbolO(String[][] a, int b, int c){
        row = b;
        column = c;
        
        if (a[row][column] != "O" && a[row][column] != "X"){
            a[row][column] = "O";
            Print(a);
            
        } else if (a[row][column] != " "){
            System.out.println("It is occupied");
            Game.printSymO();
            
        }
        
    }
    public static void Print(String[][] a){
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                    System.out.print("|" + a[i][j] + "|"); 
            }
            System.out.println();
        }
    }
}